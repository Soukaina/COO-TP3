from tp2 import *

#créer boites
def test_creationBoite():
	b = Box()


#mettre objets dans boites
def test_ajoutObjetDansBoite():
	b = Box()
	b.add("truc")
	b.add("truc2")

#objet dans la boite
def test_ObjetAjoute():
	b = Box()
	b.add("truc1")
	b.add("truc2")
	assert b.is_in("truc1")
	assert b.is_in("truc2")

#objets manquant dans la boite
def test_pasObjetDansBoite():
	b = Box()
	assert not b.is_in("truc1")
	b.add("truc1")
	assert not b.is_in("truc2")

#contents doit etre privé : _contents

#tester facilement si un truc est dans une boite
def test_trucDansBoite():
	b = Box()
	assert "truc1" not in b
	b.add("truc1")
	assert "truc1" in b

#retirer objet de la boite
def test_objetRetire():
	b = Box()
	b.add("truc1")
	b.add("truc2")
	assert "truc1" in b
	assert "truc2" in b
	b.rmv("truc2")
	assert "truc2" not in b

#ouvrir ou fermer une boite
def test_ouvertureFermeture():
	b = Box()
	assert b.is_open() == "open"
	b.close()
	assert b.is_open() == "close"
	b.open()
	assert b.is_open() == "open"

#def test_ajouterObjetDansBoiteFermeeOuOuverte():
#	b = Box()
#	b.close()
#	assert b.add("objet") == "La boite est fermée"
#	b.open()
#	assert b.add("objet")
#	assert "objet" in b

#def test_AfficherContenuBoite():
#	b = Box()
#	b.add("truc1")
#	b.add("truc2")
#	b.close
#	assert b.action_look() == "La boite est fermée"

def test_thing():
	b=Box()
	b.set_capacity(5)
	assert b.capacity() == 5

	t = Thing(3)

	assert b.has_room_fort(t)==True
	#assert t.getvolume() == 3
	t2 = Thing(7)
	b.add(t2)
	assert b.has_room_fort(t)==False
